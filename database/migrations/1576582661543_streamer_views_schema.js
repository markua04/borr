'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StreamerViewsSchema extends Schema {
  up () {
    this.create('streamer_views', (table) => {
      table.increments()
      table.integer('user_id');
      table.integer('views');
      table.timestamps()
    })
  }

  down () {
    this.drop('streamer_views')
  }
}

module.exports = StreamerViewsSchema
