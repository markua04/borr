'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments();
      table.string('username', 255).notNullable().unique();
      table.string('name', 255).notNullable();
      table.string('surname', 255).notNullable();
      table.integer('user_type', 11).notNullable();
      table.string('title', 255);
      table.text('bio',['longtext']);
      table.string('short_desc',500);
      table.string('email', 255).notNullable().unique();
      table.string('gamer_tag', 255).unique();
      table.string('profile_pic', 255);
      table.string('banner', 255);
      table.string('main_colour', 255);
      table.string('secondary_colour', 255);
      table.string('user_website', 255);
      table.string('facebook_name', 255);
      table.string('twitter_name', 255);
      table.string('instagram_name', 255);
      table.string('twitch_name', 255);
      table.string('mixer_name', 255);
      table.string('youtube_name', 255);
      table.string('country_code', 255);
      table.string('remember_token', 255);
      table.string('auth_token', 255);
      table.integer('stream_platform', 11);
      table.string('password', 255).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema;
