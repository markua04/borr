'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddIpAddressToStreamerViewsSchema extends Schema {
  up () {
    this.table('add_ip_address_to_streamer_views', (table) => {
      this.raw("ALTER TABLE streamer_views add column ip_address varchar(255) after user_id")
    })
  }

  down () {
    this.table('add_ip_address_to_streamer_views', (table) => {
      this.raw("ALTER TABLE streamer_views drop column ip_address")
    })
  }
}

module.exports = AddIpAddressToStreamerViewsSchema
