'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserActivationKeyAndActivatedColumnsSchema extends Schema {
  up () {
    this.raw("ALTER TABLE users add column activated bool after stream_platform")
    this.raw("ALTER TABLE users add column activation_key varchar(255) after activated")
  }

  down () {
    this.raw("alter table users drop column activated")
    this.raw("alter table users drop column activation_key")
  }
}

module.exports = AddUserActivationKeyAndActivatedColumnsSchema
