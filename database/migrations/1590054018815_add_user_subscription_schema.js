'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserSubscriptionSchema extends Schema {
  up () {
    this.table('add_user_subscriptions', (table) => {
      this.raw("ALTER TABLE users add column subscription bool default false after activation_key")
    })
  }

  down () {
    this.table('add_user_subscriptions', (table) => {
      this.raw("ALTER TABLE users drop column subscription")
    })
  }
}

module.exports = AddUserSubscriptionSchema
