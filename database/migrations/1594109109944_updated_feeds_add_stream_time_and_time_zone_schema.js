'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UpdatedFeedsAddStreamTimeAndTimeZoneSchema extends Schema {
  up () {
    this.table('updated_feeds_add_stream_time_and_time_zones', (table) => {
      this.raw("ALTER TABLE feeds add column timezone varchar(500) after message")
      this.raw("ALTER TABLE feeds add column stream_time varchar(255) after timezone")
    })
  }

  down () {
    this.table('updated_feeds_add_stream_time_and_time_zones', (table) => {
      this.raw("ALTER TABLE feeds drop column timezone")
      this.raw("ALTER TABLE feeds drop column stream_time")
    })
  }
}

module.exports = UpdatedFeedsAddStreamTimeAndTimeZoneSchema
