'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddHideProfileToUserSchema extends Schema {
  up () {
    this.table('add_title_and_type_to_feeds', (table) => {
      this.raw("ALTER TABLE feeds add column notification_type integer after id")
      this.raw("ALTER TABLE feeds add column feeds_title varchar(255) after user_id")
    })
  }

  down () {
    this.table('add_title_and_type_to_feeds', (table) => {
      this.raw("ALTER TABLE feeds drop column notification_type")
      this.raw("ALTER TABLE feeds drop column feeds_title")
    })
  }
}

module.exports = AddHideProfileToUserSchema
