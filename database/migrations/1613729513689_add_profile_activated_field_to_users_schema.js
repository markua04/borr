'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddProfileActivatedFieldToUsersSchema extends Schema {
  up () {
    this.table('add_profile_activated_field_to_users', (table) => {
      this.raw("ALTER TABLE users add column disable bool default false after hide_profile")
    })
  }

  down () {
    this.table('add_profile_activated_field_to_users', (table) => {
      this.raw("ALTER TABLE users drop column disable")
    })
  }
}

module.exports = AddProfileActivatedFieldToUsersSchema
