'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterUsersAddActivatedFieldSchema extends Schema {
  up () {
    this.table('alter_users_add_activated_fields', (table) => {
      // alter table
    })
  }

  down () {
    this.table('alter_users_add_activated_fields', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterUsersAddActivatedFieldSchema
