'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateGamingPlatformsSchema extends Schema {
  up () {
    this.create('gaming_platforms', (table) => {
      table.increments()
      table.string('platform_name').notNullable();
    })
  }

  down () {
    this.drop('gaming_platforms')
  }
}

module.exports = CreateGamingPlatformsSchema
