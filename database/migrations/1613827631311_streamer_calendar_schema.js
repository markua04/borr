'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StreamerCalendarSchema extends Schema {
  up () {
    this.create('streamer_calendars', (table) => {
      table.increments()
      table.integer('user_id').notNullable();
      table.string('day');
      table.string('start_time');
      table.string('end_time');
    })
  }

  down () {
    this.drop('streamer_calendars')
  }
}

module.exports = StreamerCalendarSchema
