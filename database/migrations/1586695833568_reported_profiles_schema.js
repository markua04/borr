'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReportedProfilesSchema extends Schema {
  up () {
    this.create('reported_profiles', (table) => {
      table.increments()
      table.string('reason', 255).notNullable()
      table.string('message', 500)
      table.integer('reporter_id').notNullable()
      table.integer('profile_id').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('reported_profiles')
  }
}

module.exports = ReportedProfilesSchema
