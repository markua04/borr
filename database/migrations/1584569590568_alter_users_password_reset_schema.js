'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterUsersPasswordResetSchema extends Schema {
  up () {
    this.raw("ALTER TABLE users ADD password_reset varchar(255) NULL after password")
  }

  down () {
    this.raw("alter table users drop column password_reset")
  }
}

module.exports = AlterUsersPasswordResetSchema
