'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StreamerFollowsSchema extends Schema {
  up () {
    this.create('streamer_follows', (table) => {
      table.increments()
      table.integer('follower_id');
      table.integer('streamer_id');
      table.timestamps()
    })
  }

  down () {
    this.drop('streamer_follows')
  }
}

module.exports = StreamerFollowsSchema
