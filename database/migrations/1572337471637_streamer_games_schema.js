'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StreamerGamesSchema extends Schema {
  up () {
    this.create('streamer_games', (table) => {
      table.increments();
      table.integer('user_id');
      table.integer('game_id');
      table.string('title',255);
      table.string('image',500);
    })
  }

  down () {
    this.drop('streamer_games')
  }
}

module.exports = StreamerGamesSchema
