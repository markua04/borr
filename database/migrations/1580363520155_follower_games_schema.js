'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FollowerGamesSchema extends Schema {
  up () {
    this.create('follower_games', (table) => {
      table.increments()
      table.integer('user_id');
      table.integer('game_id');
      table.string('title');
      table.string('image');
      table.timestamps()
    })
  }

  down () {
    this.drop('follower_games')
  }
}

module.exports = FollowerGamesSchema
