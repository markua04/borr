'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddHideProfileToUserSchema extends Schema {
  up () {
    this.table('add_hide_profile_to_users', (table) => {
      this.raw("ALTER TABLE users add column hide_profile bool default false after subscription")
    })
  }

  down () {
    this.table('add_hide_profile_to_users', (table) => {
      this.raw("ALTER TABLE users drop column hide_profile")
    })
  }
}

module.exports = AddHideProfileToUserSchema
