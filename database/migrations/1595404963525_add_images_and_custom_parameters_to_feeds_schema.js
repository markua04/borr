'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddImagesAndCustomParametersToFeedsSchema extends Schema {
  up () {
    this.table('add_images_and_custom_parameters_to_feeds', (table) => {
      this.raw("ALTER TABLE feeds add column image varchar(500) after stream_time")
      this.raw("ALTER TABLE feeds add column game varchar(500) after image")
    })
  }

  down () {
    this.table('add_images_and_custom_parameters_to_feeds', (table) => {
      this.raw("ALTER TABLE feeds drop column image")
      this.raw("ALTER TABLE feeds drop column game")
    })
  }
}

module.exports = AddImagesAndCustomParametersToFeedsSchema
