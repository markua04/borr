'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateUserGamingPlatformSchema extends Schema {
  up () {
    this.create('user_gaming_platforms', (table) => {
      table.increments()
      table.integer('user_id').notNullable();
      table.integer('platform_id').notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('user_gaming_platforms')
  }
}

module.exports = CreateUserGamingPlatformSchema
