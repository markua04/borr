'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NotificationTypesSchema extends Schema {
  up () {
    this.create('notification_types', (table) => {
      table.increments()
      table.string('slug')
      table.string('title')
      table.timestamps()
    })
  }

  down () {
    this.drop('notification_types')
  }
}

module.exports = NotificationTypesSchema
