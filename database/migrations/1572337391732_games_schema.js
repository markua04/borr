'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GamesSchema extends Schema {
  up () {
    this.create('games', (table) => {
      table.increments();
      table.string('title').unique();
      table.string('image');
      table.timestamps();
    })
  }

  down () {
    this.drop('games')
  }
}

module.exports = GamesSchema;
