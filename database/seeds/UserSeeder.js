'use strict'

/*
|--------------------------------------------------------------------------
| GameSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Database = use('Database')
const Hash = use('Hash')

class UserSeeder {
  async run(attributes) {
    const password = await Hash.make('P@ssword123')
    const userId = await Database
      .insert([{
        name: "Turner",
        surname: "Tenney",
        username: "Tfue",
        user_type: 2,
        title: "Game Streamer",
        bio: "Testing",
        short_desc: "Short Description",
        email: "markua04+14@gmail.com",
        gamer_tag: "Tfue",
        profile_pic: null,
        activated: true,
        password: password
      }, {
        name: "Guy",
        surname: "Beahm",
        username: "Guy",
        user_type: 2,
        title: "Game Streamer",
        bio: "Testing",
        short_desc: "Short Description",
        email: "markua04+5@gmail.com",
        gamer_tag: "Dr. Disrespect",
        profile_pic: null,
        activated: true,
        password: password
      }])
      .into('users')
  }
}

module.exports = UserSeeder
