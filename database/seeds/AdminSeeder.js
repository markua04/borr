'use strict'

/*
|--------------------------------------------------------------------------
| GameSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const Database = use('Database')
/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class UserSeeder {
  async run () {
    await Database
      .from('admins')
      .insert([
        {name: "Aiden", surname: "Markus",username: "markua04", email:"markua04@gmail.com", password: await Hash.make("@ideNM2990")},
        ]
      )
  }
}

module.exports = UserSeeder
