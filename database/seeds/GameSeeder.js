'use strict'

/*
|--------------------------------------------------------------------------
| GameSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const Database = use('Database')

class GameSeeder {
  async run () {
    const firstUserId = await Database
      .from('games')
      .insert([{title: 'Rainbow Six Siege', image: "/games/siege.jpeg"}, {title: 'Call of Duty', image: "/games/cod.jpeg"}, {title: 'Rocket League', image: "/games/rl.jpg"}])
  }
}

module.exports = GameSeeder
