'use strict'

/*
|--------------------------------------------------------------------------
| NotificationTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Database = use('Database')

class NotificationTypeSeeder {
  async run () {
    const firstUserId = await Database
      .from('notification_types')
      .insert([
        {slug: "live", title: "Going Live Update", },
        {slug: "update", title: "Status Update", },
        {slug: "admin", title: "Admin Notification", }
        ]
      )
  }
}

module.exports = NotificationTypeSeeder
