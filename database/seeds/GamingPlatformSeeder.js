'use strict'

/*
|--------------------------------------------------------------------------
| GamingPlatformSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Database = use('Database')

class GamingPlatformSeeder {
  async run () {
    await Database
      .from('gaming_platforms')
      .insert([
        {platform_name: 'Mobile'},
        {platform_name: 'Playstation'},
        {platform_name: 'PC'},
        {platform_name: 'Nintendo'},
        {platform_name: 'Xbox'}
        ])
  }
}

module.exports = GamingPlatformSeeder
