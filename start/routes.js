'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

// No Auth Routes
Route.group(() => {
  // User Routes
  Route.get('/complete-registration', 'AuthController.completeRegistration');
  Route.get('/twitter-bearer', 'AuthController.twitterBearer')
  Route.post('/password-reset', 'AuthController.passwordReset')
  Route.post('/new-password-reset', 'AuthController.newPasswordReset')
  Route.post('/register', 'AuthController.register')
  Route.post('/register-follower', 'AuthController.registerFollower')
  Route.post('/login', 'AuthController.login')
  Route.post('/logout', 'AuthController.logout')
  Route.get('/streamer-profile', 'UserController.getStreamerProfile');
  Route.get('/get-latest-streamers', 'UserController.getLatestStreamers');
  // Search Routes
  Route.get('/get-games', 'GameController.getGames');
  Route.post('search', 'SearchController.search');
  Route.post('search-gamers', 'SearchController.searchGamers');
  // Contact
  Route.post('/contact', 'ContactController.contact');
  Route.post('/recaptcha/validate', 'ContactController.recaptchaVerification');
  Route.get('/get-users-ip', 'ContactController.getUsersIpAddress');
  // Notifications
  Route.post('/notifications', 'NotificationController.getUserNotifications');
  // Gaming Platforms Routes
  Route.get('/get-games-platforms', 'UserController.getGamesPlatforms');

}).prefix('api/v1').middleware('throttle:400,1800')


// Auth Routes
Route.group(() => {
  // User routes
  Route.post('/user/edit', 'UserController.edit')
  Route.post('/get-user-games', 'StreamerGameController.getUserGames');
  Route.post('/edit-user', 'UserController.edit');
  Route.get('/get-user', 'UserController.getUser');
  Route.get('/get-user-by-token', 'UserController.getUserByToken');
  Route.get('/get-users', 'UserController.getAllUsers');
  Route.get('/get-users-by-tag', 'UserController.getUsersByTag');
  Route.get('/get-users-by-country', 'UserController.getUsersByCountry');
  Route.get('/get-users-by-tag-and-country', 'UserController.getUsersByKeyAndCountry');
  Route.get('/get-streamer-by-tag', 'UserController.getStreamerByGamerTag');
  Route.get('/get-user-dash', 'UserController.streamerDashboard');
  Route.get('/get-follower-dash', 'UserController.followerDashboard');
  Route.post('/user-image-upload', 'UserController.uploadImage');
  Route.get('/follow-streamer', 'UserController.followStreamer');
  Route.get('/unfollow-streamer', 'UserController.unfollowStreamer');
  Route.post('/feed', 'FeedController.getTodaysFeedsByUser');

  // Reported Profile Routes
  Route.post('/report-profile', 'ReportedProfileController.reportProfile');

}).prefix('api/v1').middleware('auth')


Route.group(() => {
// Admin Routes
  Route.post('/users/reported-streamers','Admin/UserController.editUser')
  Route.post('/users/edit-user', 'Admin/UserController.editUser');
  Route.post('/login', 'Admin/AuthController.login')
  Route.get('/users', 'Admin/UserController.getAllUsers')
  Route.get('/users/get-user-by-id', 'Admin/UserController.getUserById');

  Route.route('/auth/login', 'Admin/AuthController.login', ['GET','POST'])
  Route.route('/auth/logout', 'Admin/AuthController.logout', ['GET','POST'])
  Route.route('/dashboard', 'Admin/AuthController.adminDashboard', ['GET'])
  Route.route('/users/streamers', 'Admin/UserController.listStreamers', ['GET'])
  Route.route('/users/followers', 'Admin/UserController.listFollowers', ['GET'])
  Route.route('/users/update-status', 'Admin/UserController.updateUserStatus', ['POST'])

  Route.route('/user/edit/:id', 'Admin/UserController.edit', ['GET','POST'])

}).prefix('api/v1/admin')
