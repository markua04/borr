let ws = null

$(function () {
  // Only connect when username is available
  if (window.username) {
    startStatus()
  }
})

function startStatus () {
  ws = adonis.Ws().connect()

  ws.on('open', () => {
    $('.connection-status').addClass('connected')
    subscribeToChannel()
  })

  ws.on('error', () => {
    $('.connection-status').removeClass('connected')
  })
}

function subscribeToChannel () {
  const status = ws.subscribe('status')

  status.on('error', () => {
    $('.connection-status').removeClass('connected')
  })

  status.on('status_update', (message) => {
    $('.messages').append(`
      <div class="message"><h3> ${message.username} </h3> <p> ${message.body} </p> </div>
    `)
  })
}

$('#message').keyup(function (e) {
  if (e.which === 13) {
    e.preventDefault()

    const message = $(this).val()
    $(this).val('')

    ws.getSubscription('status').emit('status_update', {
      username: window.username,
      body: message
    })
    return
  }
})
