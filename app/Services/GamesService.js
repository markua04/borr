'use strict'

const GamesRepo = use('App/Repositories/GamesRepository')

class GamesService {

  constructor() {
    this.gamesRepo = new GamesRepo
  }

  async allGames() {
    return await this.gamesRepo.getAllGames()
  }

}

module.exports = GamesService
