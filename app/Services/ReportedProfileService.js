'use strict'

const ReportedProfileRepo = use('App/Repositories/ReportedProfileRepository')

class FeedService {

  constructor() {
    this.reportedProfileRepo = new ReportedProfileRepo
  }

  async save(data) {
    return this.reportedProfileRepo.create(data);
  }

  async reportedProfiles(){
    return this.reportedProfileRepo.list()
  }

}

module.exports = FeedService
