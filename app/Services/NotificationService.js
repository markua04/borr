'use strict'

const NotificationRepository = use('App/Repositories/NotificationRepository')
const Logger = use('Logger')

const live = 1;
const update = 2;
const admin = 3;

class NotificationService {

  constructor() {
    this.notificationRepo = new NotificationRepository
  }

  async createLiveNotification(data, feedsId) {
    try {
      let notificationData = {
        user_id: data.user.id,
        notification_type: live,
        feeds_id: feedsId
      }
      await this.notificationRepo.createLive(notificationData)
      return notificationData
    } catch (e) {
      Logger.error('Error : ' + error.toString())
    }
  }

  async createStatusUpdateNotification(data, feedsId) {
    let notificationData = {
      user_id: data.user.id,
      notification_type: update,
      feeds_id: feedsId
    }
    await this.notificationRepo.createStatus(notificationData)
    return notificationData
  }

  async createAdminNotification(data, feedsId) {
    let notificationData = {
      user_id: data.user.id,
      notification_type: admin,
      title: "Admin message",
      message: data.message,
      feeds_id: feedsId
    }
    await this.notificationRepo.createLive(notificationData)
    return notificationData
  }

  async getUserNotifications(data){
    let getNotifications = await this.notificationRepo.getNotificationsByFollower(data);
    let notifications = [];
    for (let i = 0; i < getNotifications.length; i++){
      notifications[i] = {
        gamer_tag: getNotifications[i].gamer_tag,
        notification_type: getNotifications[i].notification_type,
        title:getNotifications[i].feeds_title,
        message: getNotifications[i].message,
        stream_time: getNotifications[i].stream_time,
        timezone: getNotifications[i].timezone,
        game: getNotifications[i].game
      }
    }
    return notifications;
  }

}

module.exports = NotificationService
