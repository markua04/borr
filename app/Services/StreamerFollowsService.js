'use strict'

const StreamerFollowsRepo = use('App/Repositories/StreamerFollowsRepository')

class StreamerGamesService {

  constructor() {
    this.streamerFollowsRepo = new StreamerFollowsRepo
  }

  async addNewFollower(streamer,userId){
    let data = {
      streamer_id: streamer,
      follower_id: userId
    }
    return await this.streamerFollowsRepo.saveStreamerFollower(data)
  }

  async removeFollower(streamer,userId){
    return await this.streamerFollowsRepo.deleteStreamerFollower(streamer, userId)
  }

  async checkFollows(streamer,userId){
    return await this.streamerFollowsRepo.checkUserFollows(streamer, userId)
  }

  async getUserFollows(userId){
    let follows = await this.streamerFollowsRepo.userFollowsByUserId(userId)
    let following = []
    let userFollows = follows.toJSON()
    for(let i = 0;i < userFollows.length; i++)
    {
      following[i] = userFollows[i].streamer_id
    }
    return following
  }

  async streamerFollowers(streamerId){
    let followersObj = await this.streamerFollowsRepo.getUserFollowsForStreamer(streamerId)
    let followers = []
    let streamerFollowers = followersObj.toJSON()
    for(let i = 0;i < streamerFollowers.length; i++)
    {
      followers[i] = streamerFollowers[i].streamer_id
    }
    return followers
  }

  async getUserFollowsWithStreamerData(userId){
    return await this.streamerFollowsRepo.userFollowsWithStreamerDataByUserId(userId)
  }

  async userFollowsForStreamer(streamerId){
    return await this.streamerFollowsRepo.getUserFollowsForStreamer(streamerId)
  }

  async createStreamerFollowsObj(getFollows){
    let followedStreamers = getFollows.toJSON()
    let userFollows = []
    for(let i = 0; i < followedStreamers.length; i++){
      if(followedStreamers[i] !== null){
        userFollows[i] = {
          id: followedStreamers[i].streamer_id,
          streamer_name: followedStreamers[i].gamer_tag,
          streamer_pic: followedStreamers[i].profile_pic,
        }
      }
    }
    return userFollows
  }

  async createFollowingObj(following){
    let followedStreamers = following.toJSON()
    let userFollows = []
    for(let i = 0; i < followedStreamers.length; i++){
      if(followedStreamers[i] !== null){
        userFollows[i] = {
          streamer_name: followedStreamers[i].gamer_tag,
          streamer_pic: followedStreamers[i].profile_pic,
        }
      }
    }
    return userFollows
  }

}

module.exports = StreamerGamesService
