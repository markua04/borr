'use strict'

const GamingPlatformsRepository = use('App/Repositories/GamingPlatformsRepository')

class GamingPlatformsService extends GamingPlatformsRepository {

  constructor() {
    super();
    this.gamingPlatformsRepository = new GamingPlatformsRepository
  }

  async gamingPlatforms() {
    return await this.getGamingPlatforms()
  }

  async updateUserGamingPlatforms(userId, selectedGamingPlatforms) {
    await this.gamingPlatformsRepository.deleteUserGamingPlatforms(userId)
    for (let i = 0; i < selectedGamingPlatforms.length; i++) {
      await this.gamingPlatformsRepository.createUserGamingPlatforms(userId, selectedGamingPlatforms[i].id)
    }
  }

  gamingPlatformsByUser(id) {
    return this.getGamingPlatformsByUser(id)
  }

  structureGamingPlatforms(data) {
    let gamingPlatformsData = {}
    for (let i = 0; i < data.length; i++) {
      gamingPlatformsData[i] = {
        id: data[i].id,
        name: data[i].platform_name
      }
    }
    return gamingPlatformsData
  }

}

module.exports = GamingPlatformsService
