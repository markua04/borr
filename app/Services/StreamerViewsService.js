'use strict'

const StreamerViewsRepository = use('App/Repositories/StreamerViewsRepository')

class StreamerViewsService {

  constructor(){
    this.streamerViewsRepo = new StreamerViewsRepository
  }

  async createUserViews(id, ip = null){
    return await this.streamerViewsRepo.createStreamerView(id)
  }

  async userViews(id){
    let views = await this.streamerViewsRepo.getUserViews(id)
    if(views !== null){
      return views
    }
    return {}
  }

}

module.exports = StreamerViewsService
