'use strict'

const axios = require('axios')

class TwitterApiService {

  async twitterBearerToken() {
    let key = "H8WLRfDOyMjNtR6KQjK4rHwwr:9wfwjPP5YyrTBvlhInwC0IsyOeBXjIqXUUqxbtg5dJ34svnQw7";
    let basicCode = Buffer.from(key).toString('base64')
    let headers = {
      Host: "api.twitter.com",
      "User-Agent": "My Twitter App v1.0.23",
      'Authorization': "Basic " + basicCode,
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      "Content-Length": 29,
      "Accept-Encoding": "gzip"
    };
    let url = "https://api.twitter.com/oauth2/token";
    return await axios
      .post(url, {
        headers: headers,
        body: 'grant_type=client_credentials'
      })
      .then(response => {
        console.log(response.data)
        return response.data;
      })
      .catch(error => {
        console.log(error.message);
      });
  }

}

module.exports = TwitterApiService
