'use strict'

const UserRepository = use('App/Repositories/UserRepository')
const StreamerFollowsRepo = use('App/Repositories/StreamerFollowsRepository')
const Streamer = 2
const Follower = 3
const profileFilter = [
  'id',
  'gamer_tag',
  'profile_pic',
  'bio',
  'country_code',
  'instagram_name',
  'short_desc',
  'twitter_name',
  'youtube_name',
  'facebook_name',
  'banner',
  'stream_platform',
  'twitch_name',
  'user_website'
]

const searchFilter = ['users.id', 'users.gamer_tag', 'users.profile_pic', 'users.country_code']

class UserService extends UserRepository {

  constructor(){
    super()
    this.streamerFollowsRepo = new StreamerFollowsRepo
  }

  async usersByGamingPlatform(gamingPlatform, offset) {
    return await this.getUsersByGamingPlatform(gamingPlatform, offset, searchFilter)
  }

  async getUsersByGame(game, offset) {
    return await this.usersByGame(game, offset, searchFilter)
  }

  async usersByGameAndCountry(game, country, offset, user) {
    return await this.getUsersByGameAndCountry(game, country, offset, searchFilter, user)
  }

  async usersByGameAndPlatform(game, gamingPlatform, offset, user) {
    return await this.getUsersByGameAndPlatform(game, gamingPlatform, offset, searchFilter, user)
  }

  async usersByGamerTag(name, offset, user) {
    return await this.getUsersByTag(name, offset, searchFilter, user)
  }

  async allUsers() {
    return await this.getAllUsersForAdmin()
  }

  async getAllStreamers(offset, user) {
    return await this.allStreamers(offset, searchFilter, user)
  }

  async usersByCountryCode(country, offset, user) {
    return await this.getUserByCountryCode(country, offset, searchFilter, user)
  }

  async usersByCountryAndGamerTag(keyword, country, offset, user) {
    return await this.getUsersByKeyAndCountry(keyword, country, offset, searchFilter, user)
  }

  async usersByGamerTagAndCountryCodeAndGame(keyword, country, game, offset, user) {
    return await this.getUsersByGamerTagAndCountryCodeAndGame(keyword, country, game, offset, searchFilter, user)
  }

  async usersByGamerTagAndCountryCodeAndGameAndGamingPlatform(keyword, country, game, gamingPlatform, offset, user) {
    return await this.getUsersByGamerTagAndCountryCodeAndGameAndGamingPlatform(keyword, country, game, gamingPlatform, offset, searchFilter, user)
  }

  userByToken(request){
    let bearerToken = request.header("authorization")
    let token = bearerToken.replace("Bearer ", "")
    return this.getUserByToken(token)
  }

  async updateUser(data){
    let fetchUser = await this.userById(data.id);
    let user = fetchUser.toJSON()
    data.activated = 1
    data.user_type = user.user_type
    return await this.editUser(data)
  }

  latestStreamers(request){
    let count = request.get().count
    let countryCode = request.get().code
    return this.getLatestStreamers(count,countryCode)
  }

  async userByGamerTag(name){
    return await this.getUserByTag(name, profileFilter)
  }

  async usersForAdmin(type){
    if(type === "streamer"){
      type = Streamer
    } else {
      type = Follower
    }
    return await this.getAllUsersForAdmin(type)
  }

  async userById(id){
    return await this.getUserById(id)
  }

  async registerNewFollower(streamer,request){
   let user = this.userByToken(request)
   await this.registerFollower(streamer, user.id)
  }

  async createNewFollower(data, auth){
    return await this.createFollower(data)
  }

  async updateFollowerToken(email, token){
    return await this.updateFollowerTokenByEmail(email, token)
  }

  async getFollowersByParams(country,game) {
    if(country !== null && game !== null){
      return await this.followersByGameAndCountry(country,game)
    } else if(country !== null && game == null){
      return await this.followersByCountry(country,game)
    } else if(game !== null && country == null){
      return await this.followersByGame(country,game)
    }
  }

  async userFollows(id){
    return await this.streamerFollowsRepo.getUserFollows()
  }

  async adminByToken(token){
    return await this.getAdminByToken(token)
  }

  async getCurrentUserByToken(token){
    return await this.getUserByToken(token)
  }

  async userByEmail(email){
    return await this.getUserByEmail(email)
  }

  async edit(){
    return await this.editUser()
  }

  async getOffset(data, offSet){
    if(offSet !== null){
      data.offset(offSet).limit(10)
    }
    return data.paginate(1, 100)
  }

  async userByActivationKey(key){
    return await this.getUserByActivationKey(key)
  }

  async activationKey(key){
    return await this.updateActivationKey(key)
  }

  async streamersCount(){
    return await this.getStreamersCount()
  }

  async followersCount(){
    return await this.getFollowersCount()
  }

  async updateUserStatus(id, activated){
    let activate = true;
    if (activated === 0) {
      activate = false;
    }
    return await this.userStatus(id, activate)
  }

  async saveUsersProfileImageUrl(url, id){
    await this.userProfilePic(id, url)
  }

  async saveUsersBannerImageUrl(url, id){
    await this.userBannerPic(id, url)
  }
}

module.exports = UserService
