'use strict'

const FeedRepository = use('App/Repositories/FeedRepository')

class FeedService extends FeedRepository {

  constructor() {
    super();
  }

  async dailyFeed(userId, offset) {
    return await this.getFeedByUser(userId, offset)
  }

  async save(data) {
    let createData = {
      feeds_title: data.title,
      message: data.message,
      user_id: data.user.id,
      timezone: data.timezone,
      stream_time: data.datetime,
      notification_type: data.notification_type,
      image: data.image,
      game: data.game
    }
    return this.create(createData);
  }

}

module.exports = FeedService
