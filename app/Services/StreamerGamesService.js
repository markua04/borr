'use strict'

const StreamerGames = use('App/Repositories/StreamerGamesRepository')
const axios = require('axios')
const Logger = use('Logger')

class StreamerGamesService {

  constructor() {
    this.streamerGamesRepo = new StreamerGames
  }

  async gamesByUser(userId) {
    let gamesObj = []
    let games = await this.streamerGamesRepo.getUserGames(userId)
    for(let i = 0;i < games.length;i++)
    {
      gamesObj[i] = {
        id: games[i].game_id,
        name: games[i].title,
        image: games[i].image
      }
    }
    return gamesObj
  }

  async gamesIdsByUser(userId) {
    let userGames = await this.streamerGamesRepo.getUserGamesIds(userId)
    let ids = []
    for (let i = 0; i < userGames.length; i++) {
      ids[i] = userGames[i].game_id
    }
    return ids
  }

  async removeStreamerGames(id){
      return await this.streamerGamesRepo.deleteStreamerGames(id)
  }

  async createStreamerGames(data,userId){
    let i;
    for (i = 0; i < data.length; i++) {
      let image = data[i].image.original_url
      if(typeof image === 'undefined'){
        let image = await this.getGameImageById(data[i].id)
        await this.streamerGamesRepo.saveStreamerGames(data[i].id,userId,data[i].name, image)
      } else {
        await this.streamerGamesRepo.saveStreamerGames(data[i].id,userId,data[i].name, image)
      }
    }
  }

  async getGameImageById(id) {
    return axios
      .get(" https://www.giantbomb.com/api/game/" + id + "?format=json&api_key=f9db8098558b7d9911f8c81ad6fd706bea8452b8")
      .then(response => {
        return response.data.results.image.medium_url
      })
      .catch(error => {
        Logger.error('Error : %s', error)
      });
  }

}

module.exports = StreamerGamesService
