'use strict'

const Database = use('Database')
const GamingPlatform = use('App/Models/GamingPlatform')
const UserGamingPlatform = use('App/Models/UserGamingPlatform')

class GamingPlatformsRepository {

  async getGamingPlatforms(){
    let gamingPlatforms = await GamingPlatform.all();
    return gamingPlatforms.toJSON()
  }

  async create(data){
    return GamingPlatformsRepository.create(data)
  }

  async createUserGamingPlatforms(userId, platformId){
    const gamingPlatform = new UserGamingPlatform
    gamingPlatform.user_id = userId
    gamingPlatform.platform_id = platformId
    gamingPlatform.save()
  }

  getGamingPlatformsByUser(id){
    return UserGamingPlatform
      .query()
      .leftJoin('gaming_platforms', function () {
        this.on('user_gaming_platforms.platform_id', 'gaming_platforms.id')
      })
      .where({
        user_id: id
      })
      .fetch();
  }

  async deleteUserGamingPlatforms(id){
    return UserGamingPlatform
      .query()
      .where('user_id', id)
      .delete();
  }

}

module.exports = GamingPlatformsRepository
