'use strict'

const StreamerView = use('App/Models/StreamerView')

class StreamerViewRepository {

  async createStreamerView(userId, ip = null){
    let checkIfExists = StreamerView
      .query()
      .where('ip_address', ip)
      .fetch
    if(checkIfExists === null){
      let streamerView = new StreamerView
      streamerView.user_id = userId
      streamerView.ip_address = ip
      streamerView.save()
    }
  }

  async getUserViews(id) {
    return StreamerView
      .query()
      .where('user_id', id)
      .getCountDistinct('ip_address')
  }

}

module.exports = StreamerViewRepository
