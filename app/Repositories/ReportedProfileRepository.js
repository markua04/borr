'use strict'

const ReportedProfile = use('App/Models/ReportedProfile')

class ReportedProfileRepository {

  async create(data){
    let reportedProfile = new ReportedProfile
    reportedProfile.reason = data.reason
    reportedProfile.message = data.message
    reportedProfile.reporter_id = data.reporter_id
    reportedProfile.profile_id = data.profile_id
    reportedProfile.save()
  }

  async list(){

  }

}

module.exports = ReportedProfileRepository
