'use strict'

const NormalUser = 2
const Database = use('Database')
const Feed = use('App/Models/Feed')
const User = use('App/Models/User')
const StreamerFollow = use('App/Models/Feed')

class FeedRepository {

  async getFeedByUser(userId, offset){
    const currentUser = await User.findBy('id',userId)
    let follows = await StreamerFollow
      .query()
      .select('*').from('streamer_follows').where({
        follower_id: userId
      }).fetch()
    let followsData = follows.toJSON()
    let x;
    let streamers = [];
    for (x = 0;x !== followsData.length;x++) {
      streamers[x] = followsData[x]['streamer_id'];
    }
    if(currentUser.user_type === 2){
      streamers.push(userId)
    }

    let feeds = await Feed
      .query()
      .with('user')
      .whereIn('feeds.user_id', streamers)
      .orderBy('created_at', 'desc').offset(offset).limit(30).fetch()
    return feeds.toJSON()
  }

  async create(data){
    return Feed.create(data)
  }

  async getFollowsByStreamer(streamerId){
    let offset = 2;
    const currentUser = await User.findBy('id',userId)
    let follows = await StreamerFollow
      .query()
      .select('*').from('streamer_follows').where({
        follower_id: streamerId
      }).fetch()
    let followsData = follows.toJSON()
    let x;
    let streamers = [];
    for (x = 0;x !== followsData.length;x++) {
      streamers[x] = followsData[x]['streamer_id'];
    }
    if(currentUser.user_type === 2){
      streamers.push(streamerId)
    }

    let feed = await Feed
      .query()
      .with('user')
      .whereIn('feeds.user_id', streamers)
      .orderBy('created_at', 'desc')
      .fetch()
    return feed.toJSON()
  }

}

module.exports = FeedRepository
