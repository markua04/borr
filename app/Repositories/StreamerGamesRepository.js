'use strict'

const Database = use('Database')
const StreamerGame = use('App/Models/StreamerGame')

class StreamerGamesRepository {

  async saveStreamerGames(gameId,userId,title,image){
    let streamerGame = new StreamerGame
    streamerGame.user_id = userId
    streamerGame.game_id = gameId
    streamerGame.title = title
    streamerGame.image = image
    await streamerGame.save();
  }

  async getUserGames(userId){
    return await Database
      .table('streamer_games')
      .where('user_id',userId)
  }

  async getUserGamesIds(userId){
    let streamerGames = await StreamerGame
      .query()
      .from('streamer_games')
      .where('user_id',userId)
      .fetch()
    return await streamerGames.toJSON()



  }

  async deleteStreamerGames(id){
    return StreamerGame
      .query()
      .where('user_id', id)
      .delete();
  }

}

module.exports = StreamerGamesRepository
