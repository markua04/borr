'use strict'

const NormalUser = 2
const Database = use('Database')
const User = use('App/Models/User')
const Notification = use('App/Models/Notification')
const StreamerFollow = use('App/Models/Feed')

class NotificationRepository {

  async getNotificationsByFollower(follows){
    const notifications = await Notification
      .query()
      .select('*')
      .from('notifications')
      .join('users', 'notifications.user_id', 'users.id')
      .join('feeds', 'notifications.feeds_id', 'feeds.id')
      .whereIn('notifications.user_id', follows).limit(30).orderBy('notifications.created_at','desc').fetch()
    return await notifications.toJSON()
  }

  async createLive(data){
    Notification.create(data)
  }

  async createStatus(data){
    Notification.create(data)
  }

}

module.exports = NotificationRepository
