'use strict'

const Database = use('Database')

class GamesRepository {

  async getAllGames(){
    return await Database.select('*').from('games')
  }

}

module.exports = GamesRepository
