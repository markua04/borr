'use strict'

const Streamer = 2
const Follower = 3
const Database = use('Database')
const User = use('App/Models/User')
const Public = 1
const Private = 0

class UserRepository {

  async editUser(data){
    return await User
      .query()
      .where('id', data.id)
      .update(data);
  }

  async getUsersByGamingPlatform(gamingPlatform, offset, filter) {
    const users = await User
      .query()
      .select(filter)
      .innerJoin('user_gaming_platforms', 'users.id', 'user_gaming_platforms.user_id')
      .where({
        platform_id: gamingPlatform,
        user_type: Streamer,
        activated:1,
        hide_profile: false
      })
      .groupBy('users.id')
      .orderBy('users.subscription', 'desc')
      .offset(offset)
      .limit(30)
      .fetch()
    return await users.toJSON()
  }

  async getUsersByGameAndPlatform(game, gamingPlatform, offset, filter) {
    const users = await User
      .query()
      .select(filter)
      .innerJoin('user_gaming_platforms', 'users.id', 'user_gaming_platforms.user_id')
      .innerJoin('streamer_games', 'users.id', 'streamer_games.user_id')
      .where({
        platform_id: gamingPlatform,
        game_id: game,
        user_type: Streamer,
        activated:1,
        hide_profile: false
      }).orderBy('users.subscription', 'desc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getUserByTag(name, filter) {
    return User
      .query()
      .where({
        gamer_tag: name,
        user_type: Streamer,
        activated: 1,
        hide_profile: false
      }).select(filter).first()
  }

  async usersByGame(game, offset, filter) {
    const users = await User
      .query()
      .select(filter)
      .from('users').join('streamer_games', function () {
        this.on('users.id', 'streamer_games.user_id')
      }).where({
        game_id: game,
        user_type: Streamer,
        activated:1,
        hide_profile: false
      }).orderBy('users.subscription', 'desc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getUsersByTag(keyword, offset, filter, user) {
    const users = await User
      .query()
      .select(filter)
      .from('users')
      .where('gamer_tag', 'like', '%' + keyword + '%')
      .where({
        user_type: Streamer,
        activated:1,
        hide_profile: false
      })
      .orderBy('users.subscription', 'desc').offset(offset).limit(30).fetch()
      return await users.toJSON()
  }

  async getUsersByGameAndCountry(game, country, offset, filter, user) {
    const users = await User
      .query()
      .select(filter)
      .from('users')
      .innerJoin('streamer_games', 'users.id', 'streamer_games.user_id')
      .where({
        country_code: country,
        user_type: Streamer,
        activated:1,
        game_id: game,
        hide_profile: false
      })
      .orderBy('users.subscription', 'desc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getAllUsers() {
    return await User.select('*')
      .query()
      .from('users').leftJoin('streamer_views', function () {
      this.on('users.id', 'streamer_views.user_id')
    }).fetch()
  }

  async allStreamers(offset, filter, user) {
    const users = User
      .query()
      .select(filter)
      .where({
        user_type: 2,
        activated: 1,
        hide_profile: false
      })
      .orderBy('users.subscription', 'desc')
      users.offset(offset)
      return await users.limit(30).fetch()
  }

  async allFollowers(id) {
    return await Database.select('*')
      .query()
      .from('streamer_follows')
      .where({
        follower_id: id,
        hide_profile: false
      }).fetch()
  }

  async getAllUsersForAdmin(type) {
    return await User
      .query()
      .where('user_type', type)
      .fetch()
  }

  async getUsersByCountry(country, offset, filter){
    const users = await User
      .query()
      .from('users')
      .where({
        game_id: game,
        hide_profile: false
      }).select(filter)
      .orderBy('users.id', 'asc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getUsersByKeyAndCountry(keyword, country, offset, filter, user){
    const users = await User
      .query()
      .select(filter)
      .from('users')
      .where('gamer_tag', 'like', '%' + keyword + '%')
      .where({
        country_code: country,
        user_type: Streamer
      })
      .orderBy('users.id', 'asc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getUsersByGamerTagAndCountryCodeAndGame(keyword, country, game, offset, filter, user) {
    const users = await User
      .query()
      .select('users.id','users.gamer_tag','users.profile_pic')
      .from('users')
      .innerJoin('streamer_games', 'users.id', 'streamer_games.user_id')
      .where('gamer_tag', 'like', '%' + keyword + '%')
      .where({
        country_code: country,
        user_type: Streamer,
        game_id: game,
        hide_profile: false
      })
      .orderBy('users.id', 'asc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getUsersByGamerTagAndCountryCodeAndGameAndGamingPlatform(keyword, country, game, gamingPlatform, offset, filter, user) {
    const users = await User
      .query()
      .select('users.id','users.gamer_tag','users.profile_pic')
      .from('users')
      .innerJoin('streamer_games', 'users.id', 'streamer_games.user_id')
      .innerJoin('user_gaming_platforms', 'users.id', 'user_gaming_platforms.user_id')
      .where('gamer_tag', 'like', '%' + keyword + '%')
      .where({
        country_code: country,
        user_type: Streamer,
        game_id: game,
        platform_id: gamingPlatform,
        hide_profile: false
      })
      .groupBy('users.id')
      .orderBy('users.id', 'asc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async getStreamerByGamerTag({request, response}){
    let user = {};
    const gamerTag = request.get().gamer_tag;
    if(gamerTag){
      user = await User
        .query()
        .where({
          gamer_tag: gamerTag,
          user_type: Streamer,
          hide_profile: false
        })
        .fetch()
    }
    return user;
  }

  getUserByToken(token) {
    return User.findBy('auth_token', token)
  }

  async getUserByActivationKey(key) {
    return User.findBy('activation_key', key)
  }

  async getAdminByToken(token) {
    return await User
      .query()
      .where('auth_token', '=', token)
      .fetch()
  }

  async getUserByEmail(email) {
    return User.findBy('email',email)
  }

  async getUserById(id) {
    return User.findBy('id', id)
  }

  async updateUserToken(token,email) {
    await User
      .query()
      .where('email', email)
      .update({ auth_token: token })
    return User.findBy('auth_token', token)
  }

  getLatestStreamers(count = null,country = null) {
   let users = [];
    if(country !== 'none'){
      users = User
        .query()
        .where({
          country_code: country,
          user_type: Streamer,
          activated:1,
          hide_profile: false
      }).orderBy('id', 'desc')
      .limit(count)
      .fetch();
    }
    if(users <= 0){
      users = User
        .query()
        .where({
        user_type: Streamer,
        activated: 1,
        hide_profile: false
      }).limit(count).orderBy('id', 'desc').fetch()
    }
    return users
  }

  async getUserByCountryCode(country, offset, filter, user){
    const users = await User
      .query()
      .from('users')
      .where({
        user_type: Streamer,
        country_code: country,
        activated:1,
        hide_profile: false
      }).select(filter)
      .orderBy('users.id', 'asc').offset(offset).limit(30).fetch()
    return await users.toJSON()
  }

  async createFollower(data){
    const user = new User
    user.name = data.user.name
    user.surname = data.user.surname
    user.username = data.user.username
    user.email = data.user.email
    user.country_code = data.user.country_code
    user.user_type = Follower
    user.profile_pic = null
    user.banner = null
    user.password = data.user.password
    user.activated = false
    user.activation_key = data.randomString
    user.subscription = false
    await user.save()
  }

  async updateFollowerTokenByEmail(email, token){
    const user = await User.findBy('email', email)
    user.auth_token = token
    user.merge({ auth_token: token })
    user.save()
    return user
  }

  async followersByGame(game){
    return await User
      .query()
      .leftJoin('streamer_games', function () {
        this.on('users.id', 'follower_games.id')
      })
      .where({
        user_type: Follower,
        public: Public,
        country_code: country,
        hide_profile: false
      })
      .fetch()
  }

  async followersByCountry(){
    return await User
      .query()
      .where({
        user_type: Follower,
        public: Public,
        country_code: country,
        hide_profile: false
      })
      .fetch()
  }

  async followersByGameAndCountry(game,country){
    return await User
      .query()
      .innerJoin('follower_games', function () {
        this.on('users.id', 'follower_games.user_id')
      })
      .where({
        user_type: Follower,
        public: Public,
        country_code: country,
        game_id: game,
        hide_profile: false
      })
      .fetch()
  }

  async updateActivationKey(key) {
    return await User
      .query()
      .where('activation_key', key)
      .update({activated: true})
  }

  async getStreamersCount(){
    let count = await User
      .query()
      .where({
        user_type:2,
        activated:true
      })
      .fetch()
    return count.toJSON().length
  }

  async getFollowersCount(){
    let count = await User
      .query()
      .where({
        user_type:3,
        activated:true
      })
      .fetch()
    return count.toJSON().length
  }

  async hideUserTypeId(user){
    return userTypeName
  }

  async userStatus(id, activated) {
    return await User
      .query()
      .where('id', id)
      .update({
        activated: activated
      });
  }

  async userProfilePic(id, url) {
    return await User
      .query()
      .where('id', id)
      .update({
        profile_pic: url
      });
  }

  async userBannerPic(id, url) {
    return await User
      .query()
      .where('id', id)
      .update({
        banner: url
      });
  }

}

module.exports = UserRepository
