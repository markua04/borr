'use strict'

const User = use('App/Models/User')
const StreamerFollow = use('App/Models/StreamerFollow')

class StreamerFollowsRepository {

  async saveStreamerFollower(data){
    return StreamerFollow.create(data)
  }

  async deleteStreamerFollower(streamer,follower){
    let follow = StreamerFollow
      .query()
      .select('*').from('streamer_follows')
      .where({
        follower_id: follower,
        streamer_id: streamer
      }).get()
    await follow.delete()
  }

  async checkUserFollows(streamer,follower){
    return StreamerFollow
      .query()
      .select('*').from('streamer_follows')
      .where({
        follower_id: follower,
        streamer_id: streamer
      }).getCount()
  }

  async userFollowsByUserId(userId){
    return await StreamerFollow
      .query()
      .select('*').from('streamer_follows')
      .where({
        follower_id: userId
      }).fetch()
  }

  async userFollowsWithStreamerDataByUserId(userId){
   return await User
      .query()
      .innerJoin('streamer_follows', function () {
        this
          .on('users.id', 'streamer_follows.streamer_id')
      })
      .where({
        follower_id: userId
      })
      .fetch()
  }

  async getUserFollowsForStreamer(streamerId) {
    return await User
      .query()
      .innerJoin('streamer_follows', function () {
        this
          .on('users.id', 'streamer_follows.follower_id')
      })
      .where({
        streamer_id: streamerId
      })
      .fetch()
  }

}

module.exports = StreamerFollowsRepository
