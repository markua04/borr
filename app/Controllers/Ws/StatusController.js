'use strict'

const UserService = use('App/Services/UserService')
const FeedService = use('App/Services/FeedService')
const NotificationService = use('App/Services/NotificationService')
const Logger = use('Logger')
const BaseController = use('App/Controllers/Http/BaseController')

class StatusController extends BaseController {
  constructor({socket, request, auth}) {
    super()
    this.socket = socket
    this.request = request
    this.auth = auth
    this.userService = new UserService
    this.feedService = new FeedService
    this.notificationService = new NotificationService
  }

  async onUpdate(data) {
    try {
      const user = await this.userService.getCurrentUserByToken(data.auth_token)
      let notificationData = {}
      if (user) {
        let userData = await user.toJSON();
        if(userData.user_type === 2){
          data.user = userData
          let saveToFeed = await this.feedService.save(data, userData)
          let feedId = saveToFeed.toJSON().id;
          notificationData = await this.notificationService.createStatusUpdateNotification(data, feedId)
          notificationData.notification_type = data.notification_type
          notificationData.message = data.message
          notificationData.gamer_tag = userData.gamer_tag
          notificationData.user = {
            profile_pic: data.user.profile_pic
          }
          return this.socket.broadcastToAll('update', notificationData)
        } else {
          Logger.error('Error : ' + 'Follower trying to post as streamer - ID = ' + user.id + ', Name = ' + user.name + ' ' + user.surname )
          return "Error something has gone wrong!"
        }
      }
    } catch (error) {
      this.logErrors(error)
      return "Error something has gone wrong!"
    }
  }

  async onLive(data) {
    try {
      const user = await this.userService.getCurrentUserByToken(data.auth_token)
      let notificationData = {}
      if (user) {
        let userData = user.toJSON();
        if(userData.user_type === 2) {
          data.user = userData
          let saveToFeed = await this.feedService.save(data, userData)
          let feedId = saveToFeed.toJSON().id;
          notificationData = await this.notificationService.createLiveNotification(data, feedId)
          notificationData.notification_type = data.notification_type
          notificationData.message = data.message
          notificationData.gamer_tag = userData.gamer_tag
          notificationData.stream_time = data.datetime
          notificationData.timezone = data.timezone
          notificationData.game = data.game
          notificationData.user = {
            profile_pic: data.user.profile_pic
          }
        } else {
          Logger.error('Error : ' + 'Follower trying to post as streamer - ID = ' + user.id + ', Name = ' + user.name + ' ' + user.surname )
          return "Error something has gone wrong!"
        }
      }
      return this.socket.broadcastToAll('live', notificationData);

    } catch (error) {
      this.logErrors(error)
    }
  }

}

module.exports = StatusController
