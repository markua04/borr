'use strict'

const UserService = use('App/Services/UserService')
const FeedService = use('App/Services/FeedService')

class FollowController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
    this.userService = new UserService();
    this.feedService = new FeedService();
  }

  async onMessage (data) {
    const user = await this.userService.getCurrentUserByToken(data.auth_token)
    if(user){
      await this.feedService.save(user.id, data.message)
    }
    let newData = {
      message: data.message,
      user: user
    }
    return this.socket.broadcastToAll('message', newData)
  }
}

module.exports = FollowController
