'use strict'

const ReportedProfileService = use('App/Services/ReportedProfileService')

class ReportedProfileController {

  constructor() {
    this.reportedProfileService = new ReportedProfileService()
  }

  async reportedProfiles({request, auth, response}){
    const tokenPayload = auth.jwtPayload.data
    let data = request.post().body
    data.reporter_id = tokenPayload.id
    this.reportedProfileService.reportedProfiles()
    return response.status(200).json({status:true})
  }

}

module.exports = ReportedProfileController
