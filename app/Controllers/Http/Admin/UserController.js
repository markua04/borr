'use strict';

const UserService = use('App/Services/UserService')
const StreamerGamesService = use('App/Services/StreamerGamesService')
const User = require('../../../Models/User')
const {validate} = require("@adonisjs/validator/src/Validator");

class UserController {

  constructor(request){
    this.userService = new UserService
    this.streamerGamesService = new StreamerGamesService
  }

  async getAllUsers({request}) {
    try {
      const users = await this.userService.usersForAdmin(request.get().type)
      return {users:users.toJSON()}
    } catch (error) {
      return error.code
    }
  }

  async getUserById({request, response}) {
    try {
      let id = request.get().id
      const user = await this.userService.userById(id)
      return {user:user}
    } catch (error) {
      return error.code
    }
  }

  async editUser({request, response}) {
    const userData = request.post().body;
    let data = JSON.parse(userData);
    const rules = User.editUserRules(data)
    const validation = await validate(data, rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }

    await this.userService.updateUser(data)
    let user = await this.userService.userByToken(request)
    return response.status(200).json({user:user})
  }

  extractToken(request){
    let bearerToken = request.header("authorization")
    return bearerToken.replace("Bearer ", "")
  }

  async listStreamers({request}){
    try {
      return await this.userService.usersForAdmin(2)
    } catch (error) {
      return error.code
    }
  }

  async listFollowers({request}){
    try {
      return await this.userService.usersForAdmin(3)
    } catch (error) {
      return error.code
    }
  }

  async updateUserStatus({request}) {
    try {
      const userData = request.post().body;
      let data = JSON.parse(userData);
      return await this.userService.updateUserStatus(data.id, data.status)
    } catch (error) {
      return error.code
    }
  }

  async register({request, auth, response}){
    let data = JSON.parse(request.post().body)
    const validation = await validate(data.user, User.rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }

    try {
      let thorUrl = Env.get('THOR_URL')
      let randomString = this.generateString(255)
      const user = new User
      user.name = data.user.name
      user.surname = data.user.surname
      user.email = data.user.email
      user.title = data.user.title
      user.gamer_tag = data.user.gamer_tag
      user.username = data.user.username
      user.country_code = data.user.country_code
      user.user_type = Streamer
      user.profile_pic = null
      user.banner = null
      user.password = data.user.password
      user.activated = false
      user.activation_key = randomString
      await user.save()
      let userData = await User.findBy('email', data.user.email)
      let accessToken = await auth.generate(userData)
      user.auth_token = accessToken.token
      await user.save()
      await this.streamerViewsService.createUserViews(userData.id, null)
      this.welcomeEmail(userData, randomString, thorUrl)
      return response.status(200).json({user:userData,token:accessToken.token})

    } catch (error) {
      return error.code
    }
  }
}

module.exports = UserController;
