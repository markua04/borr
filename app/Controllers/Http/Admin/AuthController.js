'use strict'

const Admin = use('App/Models/Admin')
const Env = use('Env')
const Throttle = use('Throttle')
const {validate} = require("@adonisjs/validator/src/Validator");

class AuthController {

  async login ({ request, auth, response }) {
    const url = request.header('origin')
      if (url !== Env.get('ODIN_URL')) {
        return response.status(500).json({message:"Unauthorized access."})
      }

    const rules = {
      email: 'required',
      password: 'required'
    }

    const validation = await validate(request.post(), rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }
    const adminAuth = auth.authenticator("jwtAdmin");

    try {
      if (await adminAuth.attempt(request.post().email, request.post().password)) {
        let user = await this.getAdminByEmail(request.post().email);
        const accessToken = await auth.generate(user, {
          id: user.id,
          name: user.name
        })
        if(accessToken){
          user = await this.updateAdminToken(accessToken.token,request.post().email)
        }
        let userData = await this.structureUserData(user)
        return response.json({user:userData})
      }
    } catch (error) {
      let throttleMessage = "Too may login attempts. Please try again in a couple of minutes."
      if(error.message.indexOf('E_USER_NOT_FOUND') >= 0){
        let throttle = await this.throttleUser(request)
        if(throttle === true){
          return response.status(422).json({message:throttleMessage})
        }
      }
      let throttle = await this.throttleUser(request)
      if(throttle === true){
        return response.status(422).json({message:throttleMessage})
      }
      return response.status(401).json({message:"Authentication Failed"})
    }
  }

  async logout({session, response}){
    edge.global('authUser', function () {
      return null
    })
    edge.global('loggedIn', function () {
      return false
    })
    session.put('loggedIn', false)
    session.put('authUser', null)
    response.redirect('/admin/auth/login', 200)
  }

  async adminDashboard({response}){
    let streamersCount = await this.userService.streamersCount()
    let followersCount = await this.userService.followersCount()
    return response.status(200).json({
      streamersCount:streamersCount,
      followersCount:followersCount
    })
  }

  async structureUserData(user){
    return {
      id: user.id,
      name: user.name,
      surname: user.surname,
      username: user.username,
      email: user.email,
      activated: user.activated,
      auth_token: user.auth_token
    }
  }

  async updateAdminToken(token,email) {
    await Admin
      .query()
      .where('email', email)
      .update({ auth_token: token })
    return Admin.findBy('auth_token', token)
  }

  async getAdminByEmail(email) {
    return Admin.findBy('email',email)
  }

  async throttleUser(request){
    let userData = await Admin.findBy('email', request.post().email)
    if(userData){
      Throttle.resource(userData.id, 10, 400)
      if(!Throttle.attempt()) {
        return true
      }
    }
    return false
  }
}

module.exports = AuthController
