'use strict';

const GamesService = use('App/Services/GamesService')
const Logger = use('Logger')
const axios = require('axios')
const Env = use('Env')

class GameController {

  constructor(){
    this.gamesService = new GamesService
  }

  async getGames({request, response}) {
    let query = request.get().query
     return axios
      .get("https://www.giantbomb.com/api/search/?query=" + query + "&format=json&api_key=" + Env.get('GIANT_BOMB_API_KEY'))
      .then(response => {
      let gamesObj = []
      for(let i = 0;i < response.data.results.length; i++)
        {
          gamesObj[i] = {
            id: response.data.results[i].id,
            name: response.data.results[i].name,
             image: response.data.results[i].image
          }
        }
        return gamesObj
      })
      .catch(error => {
        Logger.error('Error : ' + error.code + " - " + error.message)
        Logger.error('Error : ' + error.toString())
      });
  }

  async getGameImageById(id) {
    return axios
      .get(" https://www.giantbomb.com/api/game/" + id + "?format=json&api_key=f9db8098558b7d9911f8c81ad6fd706bea8452b8")
      .then(response => {
        response.data
      })
      .catch(error => {
        Logger.error('Error : %s', error)
      });
  }
}

module.exports = GameController;

