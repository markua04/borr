'use strict';
const Database = use('Database');
class StreamerGameController {

  async save({request}) {
    const streamerGameId = await Database
      .table('streamer_games')
      .insert({user_id: request.only('user_id').user_id,game_id:request.only('game_id').game_id });
    return await streamerGameId;
  }

  userGames({ request }) {
    let userId = request.only('user_id').user_id;
    return Database
      .select('*')
      .from('streamer_games')
      .leftJoin('games', 'streamer_games.game_id', 'games.id')
      .where('user_id',userId);
  }
}

module.exports = StreamerGameController;
