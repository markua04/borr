'use strict'
const NotificationService = use('App/Services/NotificationService')
const UserService = use('App/Services/UserService')

class NotificationController {

  constructor() {
    this.notificationsService = new NotificationService;
    this.userService = new UserService;
  }

  async getUserNotifications({request, auth, response}){
    let data = request.post().body
    let userNotifications = await this.notificationsService.getUserNotifications(data);
    return response.status(200).json({notifications:userNotifications})
  }

  extractToken(request){
    let bearerToken = request.header("authorization")
    return bearerToken.replace("Bearer ", "")
  }

}

module.exports = NotificationController
