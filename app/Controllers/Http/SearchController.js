'use strict'
const UserService = use('App/Services/UserService')
const StreamerFollowsService = use('App/Services/StreamerFollowsService')
const TwitterApiService = use('App/Services/TwitterApiService')

class SearchController {

  constructor(){
    this.userService = new UserService
  }

  async search({request, auth}) {
    let user = null;
    const keyword = request.post().body.keyword
    let country = request.post().body.country
    const game = request.post().body.game
    const offset = request.post().body.offset
    const gamingPlatform = request.post().body.gaming_platform ? request.post().body.gaming_platform.id : null
    if(typeof country  === 'undefined'){
      country = null
    } else if(country === ""){
      country = null
    }
    let users = []
    if (keyword !== null && country !== null && game !== null && gamingPlatform !== null) {
      users = await this.userService.usersByGamerTagAndCountryCodeAndGameAndGamingPlatform(keyword, country, game, gamingPlatform, offset, user)
    } else if(keyword !== null && country !== null && game !== null && gamingPlatform == null){
      users = await this.userService.usersByGamerTagAndCountryCodeAndGame(keyword, country, game, offset, user)
    } else if(keyword !== null && country !== null && game == null) {
      users = await this.userService.usersByCountryAndGamerTag(keyword, country, offset, user)
    } else if(country !== null && game !== null && keyword == null){
      users = await this.userService.usersByGameAndCountry(game, country, offset, user)
    } else if(country == null && game !== null && keyword == null && gamingPlatform !== null){
      users = await this.userService.usersByGameAndPlatform(game, gamingPlatform, offset, user)
    } else if(country !== null){
      users = await this.userService.usersByCountryCode(country, offset, user)
    } else if(keyword  !== null) {
      users = await this.userService.usersByGamerTag(keyword, offset, user)
    } else if(game  !== null) {
      users = await this.userService.getUsersByGame(game, offset)
    }  else if(gamingPlatform  !== null) {
      users = await this.userService.usersByGamingPlatform(gamingPlatform, offset)
    } else {
      users = await this.userService.getAllStreamers(offset, user)
    }
    return {users: users}
  }

  async searchGamers({request}) {
    const country = request.post().body.country;
    const game = request.post().body.game;
    let users = []
    if (typeof country  !== 'undefined' && country !== null) {
      users = await this.userService.getFollowersByParams(country,game)
    }
    return {users: users}
  }

}

module.exports = SearchController
