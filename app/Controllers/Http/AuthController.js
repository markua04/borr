'use strict'

const BaseController = require('../../Controllers/Http/BaseController')
const UserRepo = require('../../Repositories/UserRepository')
const UserService = require('../../Services/UserService')
const StreamerViewsService = require('../../Services/StreamerViewsService')
const StreamerFollowsService = require('../../Services/StreamerFollowsService')
const TwitterApiService = require('../../Services/TwitterApiService')
const NotificationService = require('../../Services/NotificationService')
const GamingPlatformsService = require('../../Services/GamingPlatformsService')
const User = require('../../Models/User')
const {validate} = require("@adonisjs/validator/src/Validator");
const Streamer = 2
const Mail = use('Mail')
const Env = use('Env')
const Throttle = use('Throttle')

class AuthController extends BaseController {

  constructor(){
    super()
    this.userService = new UserService()
    this.streamerViewsService = new StreamerViewsService()
    this.streamerFollowsService = new StreamerFollowsService()
    this.twitterApiService = new TwitterApiService()
    this.notificationService = new NotificationService()
    this.gamingPlatformsService = new GamingPlatformsService()
  }

  async register({request, auth, response}){
    let data = JSON.parse(request.post().body)
    const validation = await validate(data.user, User.rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }

    try {
      let thorUrl = Env.get('THOR_URL')
      let randomString = this.generateString(255)
      const user = new User
      user.name = data.user.name
      user.surname = data.user.surname
      user.email = data.user.email
      user.title = data.user.title
      user.gamer_tag = data.user.gamer_tag
      user.username = data.user.username
      user.country_code = data.user.country_code
      user.user_type = Streamer
      user.profile_pic = null
      user.banner = null
      user.password = data.user.password
      user.activated = false
      user.activation_key = randomString
      await user.save()
      let userData = await User.findBy('email', data.user.email)
      let accessToken = await auth.generate(userData)
      user.auth_token = accessToken.token
      await user.save()
      await this.streamerViewsService.createUserViews(userData.id, null)
      this.welcomeEmail(userData, randomString, thorUrl)
      return response.status(200).json({user:userData,token:accessToken.token})

    } catch (error) {
      return error.code
    }
  }

  async welcomeEmail(userData, randomString, thorUrl){
    try {
      let template = 'emails.users.welcome-follower'

      if(userData.user_type === 2){
        template = 'emails.users.welcome-streamer'
      }

      Mail.send(template, { userData, randomString, thorUrl }, (message) => {
        message
          .to(userData.email)
          .from('info@streamfind.me')
          .subject('Welcome to StreamFind.me')
      })
    } catch (e) {
      console.log(e)
    }
  }

  async registerFollower({request, auth, response}){
    let thorUrl = Env.get('THOR_URL')
    let randomString = this.generateString(255)
    let data = JSON.parse(request.post().body)
    data.randomString = randomString
    try {
      const validation = await validate(data.user, User.followerRules)
      if (validation.fails()) {
        return response.status(422).json(validation.messages()[0])
      }
      await this.userService.createNewFollower(data, auth)
      let userData = await User.findBy('email', data.user.email)
      let accessToken = await auth.generate(userData)
      await this.userService.updateFollowerToken(data.user.email,accessToken.token)
      this.welcomeEmail(userData, randomString, thorUrl)
      return response.status(200).json({user:userData,token:accessToken.token})
    } catch (error) {
      return error.code
    }
  }

  checkIfEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async login ({ request, auth, response }) {

    if (!this.checkIfEmail(request.post().email)) {

    }

    const rules = {
      email: 'required',
      password: 'required'
    }

    const validation = await validate(request.post(), rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }

    try {
      let userFollows = []
      let streamerFollowers = {}
      let thorUrl = Env.get('THOR_URL')
      if (await auth.attempt(request.post().email, request.post().password)) {
        let user = await this.userService.userByEmail(request.post().email)
        if(user.activated === 0){
          this.welcomeEmail(user, user.activation_key, thorUrl)
          return response.status(422).json({
            message: "We sent you an email when you registered. Please open that email and click the  activate account link."
          })
        }
        if(user.disable === 1){
          return response.status(401).json({
            message: "Your account has been disabled as it has breached our guidelines."
          })
        }
        const accessToken = await auth.generate(user, {
          id: user.id,
          name: user.name,
          disable: user.disable
        })
        if(accessToken){
          let userRepo = new UserRepo()
          user = await userRepo.updateUserToken(accessToken.token,request.post().email)
        }
        if(user.user_type === Streamer){
          streamerFollowers = this.streamerFollowsService.streamerFollowers(user.id)
        }
        userFollows = await this.streamerFollowsService.getUserFollows(user.id)

        let userNotifications = await this.notificationService.getUserNotifications(userFollows);
        let gamingPlatformsData = await this.gamingPlatformsService.gamingPlatforms()

        let userData = await this.structureUserData(user)
        let gamingPlatforms = await this.structureGamingPlatforms(gamingPlatformsData)

        return response.json({user:userData, token: accessToken, followers:streamerFollowers, userFollows: userFollows, userNotifications: userNotifications, gamingPlatforms: gamingPlatforms})
      }
    } catch (error) {
      let throttleMessage = "Too may login attempts. Please try again in a couple of minutes."
      if(error.message.indexOf('E_USER_NOT_FOUND') >= 0){
        let throttle = await this.throttleUser(request)
        if(throttle === true){
          return response.status(422).json({message:throttleMessage})
        }
      }
      let throttle = await this.throttleUser(request)
      if(throttle === true){
        return response.status(422).json({message:throttleMessage})
      }
      return response.status(401).json({message:"Authentication Failed"})
    }
  }

  async throttleUser(request){
    let userData = await User.findBy('email', request.post().email)
    if(userData){
      Throttle.resource(userData.id, 10, 400)
      if(!Throttle.attempt()) {
        return true
      }
    }
    return false
  }

  async logout ({ request, response, auth }) {
    try {
      let user = await this.userService.userByToken(request)
      let saveUser = await this.userService.userById(user.id)
      saveUser.merge({ auth_token: "" })
      await saveUser.save()
      return response.status(200).json({status:true})
    } catch (error) {
      return response.status(500).json({message:"Something went wrong."})
    }
  }

  async passwordReset({request}) {
    let email = request.post().body.email
    if(email){
      let randomString = this.generateString(255)
      let userObj = await this.userService.userByEmail(email)
      userObj.merge({ password_reset: randomString })
      await userObj.save()
      let user = userObj.toJSON()
      if(user){
        try {
          Mail.send('emails.users.reset-password', { user, randomString,  }, (message) => {
            message
              .to(user.email)
              .from('info@streamfind.me')
              .subject('StreamFind.me password reset')
          })
        } catch (e) {
          console.log(e)
        }
      }
    }
  }

  async newPasswordReset({request, response}){
    try {
      let data = request.post().body
      if(data.new_password === data.confirm_password && data.uniqueCode !== null){
        let user = await User.findBy('password_reset',data.uniqueCode);
        if(user){
          user.merge({ password_reset: "", password: data.new_password })
          user.save()
          return response.status(200).json({status:true})
        }
      }
    } catch (e) {
      return response.status(500).json({message:"Something went wrong."})
    }
  }

  async twitterBearer(){
    return await this.twitterApiService.twitterBearerToken()
  }

  async completeRegistration({request, response}){
    let key = request.all()
    if(key){
      await this.userService.activationKey(key.code)
      response.status(200).json({status:true})
    } else {
      response.status(422).json({status:true})
    }
  }

  async structureUserData(user){
    return {
      id: user.id,
      instagram_name: user.instagram_name,
      main_colour: user.main_colour,
      secondary_colour: user.secondary_colour,
      name: user.name,
      surname: user.surname,
      profile_pic: user.profile_pic,
      short_desc: user.short_desc,
      stream_platform: user.stream_platform,
      subscription: user.subscription,
      youtube_name: user.youtube_name,
      twitter_name: user.twitter_name,
      twitch_name: user.twitch_name,
      username: user.username,
      user_website: user.user_website,
      user_type: user.user_type,
      title: user.title,
      gamer_tag: user.gamer_tag,
      facebook_name: user.facebook_name,
      email: user.email,
      created_at: user.created_at,
      country_code: user.country_code,
      bio: user.bio,
      banner: user.banner,
      auth_token: user.auth_token,
    }
  }

}

module.exports = AuthController
