'use strict'

// import {validate} from "@adonisjs/validator/src/Validator";
const Logger = use('Logger')
const fs = require('fs')

class BaseController {

  generateString(length) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  removeFile(path){
    try {
      fs.unlinkSync(path)
    } catch(err) {
      console.error(err)
    }
  }

  logErrors(error){
    Logger.error('------------------- Error Start --------------------')
    Logger.error('Error : ' + error.code + " - " + error.message)
    Logger.error('Error : ' + error.toString())
    Logger.error('------------------- Error End --------------------')
  }

  async structureGamingPlatforms(data) {
    let gamingPlatformsData = {}
    for (let i=0; i < data.length; i++){
      gamingPlatformsData[i] = {
        id: data[i].id,
        name: data[i].platform_name
      }
    }
    return gamingPlatformsData
  }

}

module.exports = BaseController
