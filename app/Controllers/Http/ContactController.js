'use strict'

const BaseController = require('../../Controllers/Http/BaseController')
const Mail = use('Mail')
const Contact = require('../../Models/Contact')
const {validate} = require("@adonisjs/validator/src/Validator")
const Env = use('Env')
const axios = require("axios")

class ContactController extends BaseController {

  constructor(){
    super()
  }

  async contact({request, auth, response}){

    let userData = request.post().body
    const validation = await validate(userData, Contact.rules)

    if (validation.fails()) {
      return response.status(422).json(validation.messages()[0])
    }

    if(Env.get('GOOGLE_RECAPTCHA_ENABLED') === true){
      this.recaptchaVerification(request.post().body.ip);
    }

    try {

      Mail.send('emails.users.contact-us', { userData }, (message) => {
        message
          .to(Env.get('ADMIN_EMAIL'))
          .from('info@streamfind.me')
          .subject('StreamFind contact query')
      })
      return response.status(200).json({
        status: true
      })

    } catch (e) {
      return response.status(500).json(validation.messages)
    }
  }

  async recaptchaVerification({request, response}) {
    const params = new URLSearchParams();
    params.append('secret', Env.get('GOOGLE_SECRET_KEY'));
    params.append('response', request.post().response);
    return await axios
      .post('https://www.google.com/recaptcha/api/siteverify', params)
      .then(res => {
        console.log(res)
        let status = false;
        if(res.data.success == true){
          status = true;
        }
        return response.status(200).json({
          status: status
        })
      })
      .catch(error => {
        console.log(error)
      });
  }

  async getUsersIpAddress({response}){
    return await axios
      .get('https://api.ipify.org?format=json')
      .then(res => {
        if(res.status === 200){
          return response.status(200).json({
            ip: res.data.ip
          })
        }
      })
      .catch(error => {
        return error
      });
  }

}

module.exports = ContactController
