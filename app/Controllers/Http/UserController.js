'use strict';

const UserService = use('App/Services/UserService')
const StreamerGamesService = use('App/Services/StreamerGamesService')
const StreamerFollowsService = use('App/Services/StreamerFollowsService')
const GamingPlatformsService = use('App/Services/GamingPlatformsService')
const BaseController = use('App/Controllers/Http/BaseController')
const Helpers = use('Helpers')
const fs = require("fs")
const {validate} = require("@adonisjs/validator/src/Validator");
const Logger = use('Logger')
const Helper = use('App/Common')

class UserController extends BaseController {

  constructor() {
    super()
    this.userService = new UserService
    this.streamerGamesService = new StreamerGamesService
    this.streamerFollowsService = new StreamerFollowsService
    this.gamingPlatformsService = new GamingPlatformsService
  }

  async getUserByTag({request, response}) {
    try {
      let name = request.get().name
      const user = await this.userService.usersByGamerTag(name)
      const userId = user[0].id
      const games = await this.streamerGamesService.gamesByUser(userId)
      return {user: user[0], games: games}
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getAllUsers() {
    try {
      const users = await this.userService.allUsers()
      return {users: users}
    } catch (error) {
      this.logErrors(error)
    }
  }

  async getUsersByTag({request, response}) {
    return this.userService.usersByGamerTag(request)
  }

  async getUsersByCountry({request, response}) {
    try {
      let users = {};
      const country = request.get().country;
      if (country) {
        users = await this.userService.usersByCountryCode()
      }
      return users;
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getUsersByKeyAndCountry({request, response}) {
    let users = [];
    const keyword = request.get().keyword;
    const country = request.get().country;
    if (keyword && country) {
      await this.userService.usersByCountryAndGamerTag(keyword, country)
    }
    return users;
  }

  async getStreamerByGamerTag({request, response}) {
    try {
      let user = {};
      const gamerTag = request.get().gamer_tag;
      if (gamerTag) {
        return await this.userService.userByGamerTag(gamerTag)
      }
      return user;
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getUser({request, response}) {
    try {
      let user = await this.userService.userByToken(request.header('authorization'))
      let games = await this.userService.gamesByUser(user.id)
      return {user: user[0], games: games};
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async edit({request, response}) {
    try {
      let userRequest = request.post().body;
      let data = JSON.parse(userRequest);
      const rules = {
        username: 'required|unique:users,username,id,' + data.user.id,
        name: 'required',
        surname: 'required',
        email: 'required|unique:users,email,id,' + data.user.id,
        title: 'required',
        gamer_tag: 'required|unique:users,gamer_tag,id,' + data.user.id
        // country_code: 'required'
      }

      const validation = await validate(data.user, rules)

      if (data.select_gaming_platforms && data.select_gaming_platforms.length < 1) {
        let validateMessage = {
          message: 'Gaming platform',
          field: 'gaming_platforms',
          validation: 'required'
        }
        return response.status(422).json(validateMessage)
      }

      if (validation.fails()) {
        return response.status(422).json(validation.messages()[0])
      }

      if (data.user_data.stream_platform) {
        data.user.stream_platform = data.user_data.stream_platform.id
      }
      await this.userService.updateUser(data.user)
      if (data.select_gaming_platforms) {
        await this.gamingPlatformsService.updateUserGamingPlatforms(data.user.id, data.select_gaming_platforms)
      }
      let token = this.extractToken(request)
      let selectedGames = data.user_data.games
      if (selectedGames !== null) {
        let currentUserGamesIds = await this.streamerGamesService.gamesIdsByUser(data.user.id)
        let updateGames = false;
        for (let i = 0; i < selectedGames.length; i++) {
          if (!currentUserGamesIds.includes(selectedGames[i].id)) {
            updateGames = true;
            break;
          }
        }
        if (updateGames === true) {
          await this.streamerGamesService.removeStreamerGames(data.user.id)
          await this.streamerGamesService.createStreamerGames(data.user_data.games, data.user.id)
        }
      }
      return response.status(200).json({
        user: data.user,
        auth_token: token,
        gaming_platforms: data.select_gaming_platforms
      })
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getUserByToken({request, response}) {
    try {
      let user = await this.userService.userByToken(request)
      let games = await this.streamerGamesService.gamesByUser(user.id)
      let gamingPlatforms = await this.gamingPlatformsService.gamingPlatformsByUser(user.id)
      let structuredGamingPlatforms = await this.gamingPlatformsService.structureGamingPlatforms(gamingPlatforms.toJSON())
      return {user: user, games: games, gaming_platforms: structuredGamingPlatforms}
    } catch (error) {
      Logger.transport('file').info(error.message, request.url())
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getLatestStreamers({request, response}) {
    try {
      let users = await this.userService.latestStreamers(request)
      return response.status(200).json(users)
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async uploadImage({request, response}) {
    try {
      let helper = new Helper
      const image = request.file('image')
      const userId = request.post().id
      const type = request.post().type
      if (image) {
        const imageName = this.generateString(10) + "." + image.extname
        return await image.move(Helpers.publicPath('uploads'), {
          name: imageName,
          overwrite: true
        }).then(() => {
          const fileContent = fs.readFileSync(Helpers.publicPath('uploads') + "/" + imageName)
          const base64data = Buffer.from(fileContent, 'base64');
          helper.bucketUpload(base64data, imageName, type, userId)
          return response.status(200).json({ message: "Image uploaded successfully!" })
        }).finally(() => {
          this.removeFile(Helpers.publicPath('uploads') + "/" + imageName)
        })
      }
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  extractToken(request) {
    let bearerToken = request.header("authorization")
    return bearerToken.replace("Bearer ", "");
  }

  async getStreamerProfile({request, response}) {
    try {
      let name = request.get().name
      const rules = {
        name: 'required'
      }
      const validation = await validate(request.get(), rules)

      const user = await this.userService.userByGamerTag(name)
      const userData = user.toJSON()
      const userId = userData.id
      const games = await this.streamerGamesService.gamesByUser(userId)
      return {user: user, games: games}
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async streamerDashboard({request, response, auth}) {
    try {
      const tokenPayload = auth.jwtPayload.data
      const user = await this.userService.userById(tokenPayload.id)
      let streamerFollows = await this.streamerFollowsService.userFollowsForStreamer(user.id)
      let getFollows = await this.streamerFollowsService.getUserFollowsWithStreamerData(user.id)
      let userFollows = await this.streamerFollowsService.createStreamerFollowsObj(getFollows)
      return response.status(200).json({user: user, followers: streamerFollows, follows: userFollows})
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async followerDashboard({request, response, auth}) {
    try {
      const tokenPayload = auth.jwtPayload.data
      const user = await this.userService.userById(tokenPayload.id)
      let getFollows = await this.streamerFollowsService.getUserFollowsWithStreamerData(user.id)
      let userFollows = await this.streamerFollowsService.createStreamerFollowsObj(getFollows)
      return response.status(200).json({user: user.toJSON(), follows: userFollows})
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async followStreamer({request, response}) {
    try {
      let message = "";
      let streamerGt = request.get().gamer_tag;
      let user = await this.getUserByToken({request})
      let userId = user.user.id
      let streamer = await this.userService.usersByGamerTag(streamerGt)
      if (userId && userId !== streamer[0].id) {
        let check = await this.streamerFollowsService.checkFollows(streamer[0].id, user.user.id)
        if (check === 0) {
          await this.streamerFollowsService.addNewFollower(streamer[0].id, user.user.id)
          message = "You are now following " + streamer[0].gamer_tag + "!"
        } else {
          message = "You are already following " + streamer[0].gamer_tag
        }
        let follows = await this.streamerFollowsService.getUserFollows(userId)
        return response.status(200).json({status: true, message: message, follows: follows})
      } else {
        message = "You may not follow yourself!"
        return response.status(200).json({status: true, message: message})
      }
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async unfollowStreamer({request, response}) {
    try {
      let message = "";
      let streamerGt = request.get().gamer_tag;
      let user = await this.getUserByToken({request})
      let userId = user.user.id
      let streamer = await this.userService.usersByGamerTag(streamerGt)
      if (userId && userId !== streamer[0].id) {
        await this.streamerFollowsService.removeFollower(streamer[0].id, user.user.id)
        message = "You are no longer following " + streamer[0].gamer_tag + "!"
      }
      let follows = await this.streamerFollowsService.getUserFollows(userId)
      return response.status(200).json({status: true, message: message, follows: follows})
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }

  async getGamesPlatforms({response}) {
    try {
      let gamingPlatformsData = await this.gamingPlatformsService.gamingPlatforms()
      return await this.structureGamingPlatforms(gamingPlatformsData)
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message: "Something went wrong."})
    }
  }
}

module.exports = UserController;
