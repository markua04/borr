'use strict';

const FeedService = use('App/Services/FeedService')
const BaseController = use('App/Controllers/Http/BaseController')

class FeedController extends BaseController {

  constructor() {
    super();
    this.feedService = new FeedService();
  }

  async getTodaysFeedsByUser({auth, request, response}){
    try {
      const offset = request.post().body.offset
      const tokenPayload = auth.jwtPayload.data
      return await this.feedService.dailyFeed(tokenPayload.id, offset)
    } catch (error) {
      this.logErrors(error)
      return response.status(500).json({message:"Something went wrong."})
    }
  }

}

module.exports = FeedController;
