'use strict'

class AuthChecker {
  async handle ({ request, response, auth }, next) {
    if(auth.jwtPayload.data.disable) {
      return response.status(401).json("Account deactivated!")
    } else {
      await next()
    }
  }
}

module.exports = AuthChecker
