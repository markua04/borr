'use strict';
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Env = use('Env');
class IpChecker {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Funct eion} next
   */
  async handle ({ request }, next) {
    if(request.request.connection.remoteAddress){
      await next()
    } else {
      return "failed!";
    }
    // call next to advance the request
  }
}

module.exports = IpChecker;
