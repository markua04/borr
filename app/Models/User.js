'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  streamerFollows () {
    return this.hasMany('App/Models/StreamerFollow','id','streamer_id')
  }

  streamerViews () {
    return this.hasOne('App/Models/StreamerView')
  }

  static get createdAtColumn () {
    return 'created_at'
  }

  static get updatedAtColumn () {
    return 'updated_at'
  }

  static get rules() {
    return {
      username: 'required|min:5|unique:users,username,id',
      name: 'required',
      surname: 'required',
      email: 'required|unique:users,email',
      title: 'required',
      gamer_tag: 'unique:users,gamer_tag',
      user_type: 'required',
      country_code: 'required',
      password: 'required',
    }
  }

  static get followerRules() {
    return {
      username: 'required',
      name: 'required',
      surname: 'required',
      email: 'unique:users,email',
      user_type: 'required',
      country_code: 'required',
      password: 'required',
    }
  }

  static editUserRules(user) {
    return {
      username: `required|min:5|unique:users,username,id,${user.id}`,
      name: 'required',
      surname: 'required',
      email: `required|unique:users,email,id,${user.id}`
    }
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  static get hidden () {
    return ['password']
  }
}

module.exports = User
