'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class Contact extends Model {

  static get createdAtColumn () {
    return 'created_at'
  }

  static get updatedAtColumn () {
    return 'updated_at'
  }

  static boot () {
    super.boot()
  }

  static get rules() {
    return {
      name: 'required',
      surname: 'required',
      email: 'required|email',
      phone_number: 'required',
      message: 'required',
    }
  }
}

module.exports = Contact
