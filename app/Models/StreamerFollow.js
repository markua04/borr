'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StreamerFollow extends Model {

  userModel () {
    return this.hasMany('App/Models/User','id','follower_id')
  }

}

module.exports = StreamerFollow
