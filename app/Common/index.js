'use strict'

const AWS = require("aws-sdk");
const UserService = use('App/Services/UserService')
const Logger = use('Logger')

module.exports = class Helper {

  constructor() {
    this.userService = new UserService
  }

  async bucketUpload(file, name, type = "", userId) {
    AWS.config.region = process.env.AWS_REGION;
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: process.env.AWS_IDENTITY_POOL,
    });
    const s3Service = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      params: {
        Bucket: process.env.AWS_BUCKET_NAME,
        CreateBucketConfiguration: {
          LocationConstraint: AWS.config.region
        }
      }
    })
    const albumPhotosKey = encodeURIComponent('Users') + "/"
    const photoKey = albumPhotosKey + name
    const uploadParams = {Bucket: process.env.AWS_BUCKET_NAME, Key: photoKey, Body: file}
    try {
      return s3Service.upload(uploadParams, (err, data) => {
        if (data) {
          if (type === 'profile_pic') {
            this.saveUsersProfilePicUrl(data.Location, userId)
          } else {
            this.saveUsersBannerPicUrl(data.Location, userId)
          }
        }
      })
    } catch (error) {
      Logger.error('------------------- Error Start --------------------')
      Logger.error('Error : ' + error.code + " - " + error.message)
      Logger.error('Error : ' + error.toString())
      Logger.error('------------------- Error End --------------------')
    }
  }

  saveUsersProfilePicUrl(url, userId) {
    return this.userService.saveUsersProfileImageUrl(url, userId)
  }

  saveUsersBannerPicUrl(url, userId) {
    return this.userService.saveUsersBannerImageUrl(url, userId)
  }
}
