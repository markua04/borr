#!/bin/bash
echo "Deploying Borr API"

git pull origin master
chmod +x deploy_api.sh

npm install

pm2 restart server
pm2 show server

echo "Deployment done!"
