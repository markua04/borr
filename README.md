# StreamFind Setup Info

## Setup

Use the adonis command to install the blueprint

manually clone the repo and then run `npm install`.

### Environment File
```js
Copy and paste the .env.example file and rename it to .env
then add your credentials as needed.
```

### Generate App Key
```js
adonis key:generate
```

### Environment File
```js
Create a mysql database with the name you chose for in the .env file
```

### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
